import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Controls.Material 2.15
import QtQuick.Layouts 1.15

import "FontAwesome"

Page {
    id: rootItem
    width: 400; height: 400
    Material.theme: Material.Dark
    Material.accent: Material.Teal

    property int currentIndex: -1

    ListModel {
        id: myModel
        ListElement { name: "Item 1" }
    }

    ColumnLayout {
        anchors { fill: parent; margins: 10 }
        ListView {
            id: listView
            Layout.fillWidth: true; Layout.fillHeight: true
            model: myModel
            spacing: 3
            clip: true
            delegate: Frame {
                width: parent.width
                padding: 0; bottomPadding: 0; topPadding: 0
                Material.elevation: 3
                SwipeDelegate {
                    RowLayout {
                        spacing: 12
                        anchors { verticalCenter: parent.verticalCenter;  left: parent.left }
                        anchors.leftMargin: 12 + ((parent.swipe.position !== 0) ?
                                                      parent.swipe.position*parent.swipe.leftItem.width:0)
                        Label { font { family: FontAwesome.solid; styleName: "Solid" } text: Icons.faUser }
                        Label { text: name }
                    }
                    width: parent.width
                    swipe.left: Label {
                        height: parent.height
                        verticalAlignment: Label.AlignVCenter
                        padding: 12
                        text: "Delete"
                        background: Rectangle { color: "tomato" }
                        SwipeDelegate.onClicked: myModel.remove(index)
                    }
                    onClicked: {
                        currentIndex = index
                        textField.text = myModel.get(index).name
                        addDialog.open()
                    }
                }
            }
            add: Transition {
                NumberAnimation { properties: "x"; from: rootItem.width; duration: 500; easing.type: Easing.OutBounce }
            }
            displaced: Transition {
                NumberAnimation { properties: "y"; duration: 500; easing.type: Easing.OutBounce }
            }
        }
        Button {
            Layout.alignment: Qt.AlignHCenter
            text: "Add element"
            onClicked: {
                currentIndex = -1
                textField.clear()
                addDialog.open()
            }
        }
    }
    Dialog {
        Material.theme: Material.Dark
        Material.accent: Material.Teal

        id: addDialog
        width: parent.width/2
        anchors.centerIn: parent
        title: ((currentIndex === -1) ? "Add":"Edit") + " Element"
        standardButtons: Dialog.Ok | Dialog.Cancel
        onVisibleChanged: textField.forceActiveFocus()
        TextField {
            id: textField
            width: parent.width
            placeholderText: "Digite o nome do elemento"
        }
        onAccepted: {
            if (currentIndex === -1)
                myModel.insert(0, { "name": textField.text })
            else
                myModel.setProperty(currentIndex, "name", textField.text)
        }
    }
}
