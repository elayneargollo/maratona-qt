# How to run this example with real-time data

Get your appid at https://openweathermap.org/ and configure it in restful-dashboard.js

# How to build this example

1. Download emsdk

git clone https://github.com/emscripten-core/emsdk.git

2. Move into emsdk directory:

cd emsdk

3. Check at https://doc.qt.io/qt-5/wasm.html the known-good Emscripten version for you installed Qt version and install it. For Qt 5.15, this is Emscripten 1.39.8.

./emsdk install 1.39.8 

4. Activate this emscripten version:

./emsdk activate 1.39.8

5. Run the emsdk configuration script:

source emsdk_env.sh

6. Build the example

cd ..
mkdir build
cd build
<path-to-qt-for-web-assembly-qmake> ..
make

7. Run a quick-setup python HTTP server:

python -m http.server 8080

8. Or, preferably for a production environment, start a nginx server with gzip support:

docker run -d -p 8080:80 -v $PWD:/usr/share/nginx/html:ro -v $PWD/../nginx.conf:/etc/nginx/nginx.conf nginx

9. Finally, open a web browser and run this example at http://localhost:8080/restful-dashboard-mobile.html
