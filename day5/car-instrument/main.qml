import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Controls.Material 2.15 as M
import QtQuick.Window 2.15
import QtQuick.Layouts 1.15
import QtQuick3D 1.15
import QtQuick3D.Helpers 1.15

ApplicationWindow {
    visible: true
    visibility: Window.FullScreen
    title: qsTr("Hello World")
    color: "black"

    M.Material.foreground: "white"

    DefaultMaterial {
        id: tireFailure
        diffuseColor: "green"
        SequentialAnimation on diffuseColor {
            loops: Animation.Infinite
            running: true
            ColorAnimation { from: "black"; to: "red"; duration: 500 }
            ColorAnimation { from: "red"; to: "black"; duration: 500 }
        }
    }

    ColumnLayout {
        anchors { left: parent.left; top: parent.top; leftMargin: 10; topMargin: 10 }
        spacing: -5
        Button {
            Layout.fillWidth: true; text: "Topo"
            onClicked: { camX.value = 0; camY.value = 0; camZ.value = 0; scaleSlider.value = 1.45 }
        }
        RowLayout {
            Button {
                text: "Direito"
                Layout.preferredWidth: leftButton.width
                onClicked: { camX.value = 0; camY.value = -90; camX.value = 0; scaleSlider.value = 0.75 }
            }
            Button {
                id: leftButton
                text: "Esquerdo"
                onClicked: { camX.value = 0; camY.value = 90; camZ.value = 0; scaleSlider.value = 0.75 }
            }
        }
        RowLayout {
            Button {
                Layout.preferredWidth: leftButton.width
                text: "Frente"
                onClicked: { camX.value = 75; camY.value = 0; camZ.value = 0; scaleSlider.value = 0.75 }
            }
            Button {
                Layout.preferredWidth: leftButton.width
                text: "Trás"
                onClicked: { camX.value = -100; camY.value = -180; camZ.value = 0; scaleSlider.value = 0.6 }
            }
        }
    }

    ColumnLayout {
        anchors { right: parent.right; top: parent.top; rightMargin: 10; topMargin: 10 }
        spacing: -5
        GroupBox {
            title: "Tire failure simulation"
            M.Material.background: "#323232"
            Layout.fillWidth: true
            ColumnLayout {
                Layout.fillWidth: true
                CheckBox {
                    text: "Rear left"
                    onCheckedChanged: car.tYRE_MESH_Reduced_003.materials[4] = (checked) ?tireFailure:car.tireMaterial
                }
                CheckBox {
                    text: "Rear right"
                    onCheckedChanged: car.tYRE_MESH_Reduced_004.materials[4] = (checked) ?tireFailure:car.tireMaterial
                }
                CheckBox {
                    text: "Front left"
                    onCheckedChanged: car.tYRE_MESH_Reduced_002.materials[4] = (checked) ?tireFailure:car.tireMaterial
                }
                CheckBox {
                    text: "Front right"
                    onCheckedChanged: car.tYRE_MESH_Reduced_001.materials[4] = (checked) ?tireFailure:car.tireMaterial
                }
            }
        }
        CheckBox {
            Layout.fillWidth: true; text: "Inspect"
            onCheckedChanged: {
                car.engineHood.z = checked ? car.engineHood.z+0.5:car.engineHood.z-0.5
                car.door_Mesh_Reduced_001.x = checked ? car.door_Mesh_Reduced_001.x+0.5:car.door_Mesh_Reduced_001.x-0.5
                car.door_Mesh_Reduced_002.x = checked ? car.door_Mesh_Reduced_002.x+0.5:car.door_Mesh_Reduced_002.x-0.5
                car.door_Mesh_Reduced_003.x = checked ? car.door_Mesh_Reduced_003.x-0.5:car.door_Mesh_Reduced_003.x+0.5
                car.door_Mesh_Reduced_004.x = checked ? car.door_Mesh_Reduced_004.x-0.5:car.door_Mesh_Reduced_004.x+0.5
                car.fullsize_doorglass_FL.x = checked ? car.fullsize_doorglass_FL.x+0.5:car.fullsize_doorglass_FL.x-0.5
                car.fullsize_doorglass_RL.x = checked ? car.fullsize_doorglass_RL.x+0.5:car.fullsize_doorglass_RL.x-0.5
                car.fullsize_doorglass_FR.x = checked ? car.fullsize_doorglass_FR.x-0.5:car.fullsize_doorglass_FR.x+0.5
                car.fullsize_doorglass_RR.x = checked ? car.fullsize_doorglass_RR.x-0.5:car.fullsize_doorglass_RR.x+0.5
            }
        }
    }

    View3D {
        anchors.fill: parent
        renderMode: View3D.Offscreen

        environment: SceneEnvironment {
            antialiasingMode: SceneEnvironment.MSAA
            antialiasingQuality: SceneEnvironment.VeryHigh
        }

        Car {
            id: car
            mainNode.eulerRotation: Qt.vector3d(rotX.value, rotY.value, rotZ.value)
        }

        AxisHelper {
            enableAxisLines: false
        }

        Node {
            id: camera
            PerspectiveCamera {
                position: Qt.vector3d(0, 0, 735).times(scaleSlider.value)
                fieldOfView: 59.2978
                Behavior on position { Vector3dAnimation { duration: 500 } }
            }
            eulerRotation: Qt.vector3d(camX.value, camY.value, camZ.value)
            Behavior on eulerRotation { Vector3dAnimation { duration: 500 } }
        }

        ColumnLayout {
            id: layout
            spacing: -15
            anchors { left: parent.left; bottom: parent.bottom; leftMargin: 10; bottomMargin: 10 }
            RowLayout { Label { text: "Model x rotation" } Slider { id: rotX; from: 0; to: 360 } Label { text: Math.trunc(rotX.value) } }
            RowLayout { Label { text: "Model y rotation" } Slider { id: rotY; from: 0; to: 360 } Label { text: Math.trunc(rotY.value) } }
            RowLayout { Label { text: "Model z rotation" } Slider { id: rotZ; from: 0; to: 360 } Label { text: Math.trunc(rotZ.value) } }
            RowLayout { Label { text: "Zoom" } Slider { id: scaleSlider; from: 0; to: 3; value: 1.45 } Label { text: scaleSlider.value } }
            RowLayout { Label { text: "Camera x rotation" } Slider { id: camX; from: -360; to: 360 } Label { text: Math.trunc(camX.value) } }
            RowLayout { Label { text: "Camera y rotation" } Slider { id: camY; from: -360; to: 360 } Label { text: Math.trunc(camY.value) } }
            RowLayout { Label { text: "Camera z rotation" } Slider { id: camZ; from: -360; to: 360 } Label { text: Math.trunc(camZ.value) } }
            Label { text: "Camera: " + camera.children[0].position + " - " + camera.eulerRotation; color: "white"; Layout.topMargin: 15 }
        }
    }
}
